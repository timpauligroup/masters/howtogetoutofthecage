(
SynthDef(\omgverb, {
	|in, out, wet|
	var dry = In.ar(in, 1);
	var sig = FreeVerb.ar(dry, wet, 1, 1);
	Out.ar(out, sig);
}).add;
)
